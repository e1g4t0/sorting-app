package com.epam.project;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        Sorting sorting = new Sorting();

        String result = sorting.sort(input);

        System.out.println(result);

    }


}
