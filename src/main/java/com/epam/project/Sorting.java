package com.epam.project;

import java.util.Arrays;

public class Sorting {

    public String sort(String input) {


        if(input == null){
            throw new IllegalArgumentException();
        }

        if (input.length() == 0){
            throw new IllegalArgumentException();
        }

        String[] arrayString = input.split(" ");

        int []array = new int[arrayString.length];

        try{
            for (int i = 0; i < arrayString.length; i++) {
                array[i] = Integer.parseInt(arrayString[i]);
            }
        }
        catch (IllegalArgumentException e){
            throw new IllegalArgumentException();
        }

        int j;
        int drop;

        for (int i = 1; i < array.length; ++i) {
            j = 0;
            while(j != i && array[i - j] < array[i - 1 - j]){
                drop = array[i - j];
                array[i - j] = array[i - 1 - j];
                array[i - 1 - j] = drop;
                ++j;
            }
        }

        return Arrays.toString(array).replaceAll("\\[|\\]|,|", "");
    }
}
