package com.epam.project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingTest {
    private static final Logger logger = LoggerFactory.getLogger(SortingTest.class);

    @Parameterized.Parameters(name = "{index} => in={0}, out={1}")
    public static Collection<Object[]> otherCases(){
        return Arrays.asList(new Object[][]{
                {"1 8 0 1 4", "0 1 1 4 8"},
                {"1 1 1 1 1 1 1", "1 1 1 1 1 1 1"},
                {"8 0 23 434 144 2234 423432 8977 32444 234 -1 -4 324 324 234 7982 234 333", "-4 -1 0 8 23 144 234 234 234 324 324 333 434 2234 7982 8977 32444 423432"},
                {"-1 -3 -324 -324 -53 -12 -543 -34 -533 -134 -999 -1111 -309", "-1111 -999 -543 -533 -324 -324 -309 -134 -53 -34 -12 -3 -1"},
                {"1 2", "1 2"},
                {"0 1 2 3 4 5 6 7 8", "0 1 2 3 4 5 6 7 8"},
        });
    }

    String input;
    String expected;
    Sorting sorting = new Sorting();

    public SortingTest(String input, String expected){
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void testOtherCases(){
        logger.info("Test method executed successfully");
        assertEquals(expected, sorting.sort(input));
    }

}
