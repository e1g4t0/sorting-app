package com.epam.project;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SortingTestCornerCases {
    private static final Logger logger = LoggerFactory.getLogger(SortingTestCornerCases.class);

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyCase(){
        logger.info("Empty corner case has been executed successfully");
        String input = "";
        sorting.sort(input);
    }

    @Test
    public void testSingleElementArrayCase(){
        logger.info("Single element corner case has been executed successfully");
        String input = "5";
        Assert.assertEquals("5", sorting.sort(input));
    }

    @Test
    public void testTenElementsArrayCase(){
        logger.info("Ten elements corner case has been executed successfully");
        String input = "5 3 1 9 12 234 100 233 0 6";
        Assert.assertEquals("0 1 3 5 6 9 12 100 233 234", sorting.sort(input));
    }

    @Test
    public void testMoreThanTenElementsArrayCase(){
        logger.info("More than ten elements corner case has been executed successfully");
        String input = "7 42 98 23 988 222 11 0 9 4 3 0 4";
        Assert.assertEquals("0 0 3 4 4 7 9 11 23 42 98 222 988", sorting.sort(input));
    }


}
