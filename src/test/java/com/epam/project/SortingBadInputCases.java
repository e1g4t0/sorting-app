package com.epam.project;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SortingBadInputCases {
    private static final Logger logger = LoggerFactory.getLogger(SortingBadInputCases.class);

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        logger.info("Null case has been executed successfully");
        String input = null;
        sorting.sort(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyCase(){
        logger.info("Empty case has been executed successfully");
        String input = "";
        sorting.sort(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFractionalCase(){
        logger.info("Fractional case has been executed successfully");
        String input = "1 3 4 5.5";
        sorting.sort(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonDigitalCase(){
        logger.info("Non digital case has been executed successfully");
        String input = "ad fds fs fd";
        sorting.sort(input);
    }
}
